# PPRL vector match service

This service provides matching capabilities for Bloom filter based PPRL.

## Installation

To run an ephemeral container, use the line below.

```
docker run --rm -p 8080:8080 -d quay.io/mds4ul/pprl-match:latest
```

You can also compile this project by hand and run the standalone executable JAR.
Dependencies are stored in `target/libs` and are referenced in the JAR manifest file.

```
mvn clean package
cd target
java -jar pprl-match-service-1.0-SNAPSHOT.jar
```

## Configuration

You can configure the service by manually overwriting the [app.properties](src/main/resources/app.properties) file.
It is also possible to define system properties when running the service on the command line.

**Option** | **System property** | **Description** | **Default**
:--|:--|:--|:--
`batch-size` | `-Dservice.batch-size` | Batches of domain and range vectors to process at a time | 100
`max-comparison-count` | `-Dservice.max-comparison-count` | Maximum amount of bit vector pairs to compare (if -1, the upper limit is the highest 64-bit signed integer) | -1
`use-available-core-count` | `-Dservice.use-available-core-count` | If set, uses the available processor count to determine the size of the match thread pool | true
`worker-thread-count` | `-Dservice.worker-thread-count` | Amount of threads to use in the match thread pool (only if `use-available-core-count` is false) | 2

## Example request

```python
import json
import requests

r = requests.post("http://localhost:8080", json={
    "config": {
        "measure": "jaccard",
        "threshold": 0.9
    },
    "domain": [
        {
            "id": "d001",
            "value": "SGVsbG8sIHdvcmxkIQ=="
        }
    ],
    "range": [
        {
            "id": "r001",
            "value": "SGFsbG8sIHdvcmxkIQ=="
        }
    ]
})

print(json.dumps(r.json(), indent=2))
# {
#   "config": {
#     "measure": "jaccard",
#     "threshold": 0.9
#   },
#   "matches": [
#     {
#       "domain": {
#         "id": "d001",
#         "value": "SGVsbG8sIHdvcmxkIQ=="
#       },
#       "range": {
#         "id": "r001",
#         "value": "SGFsbG8sIHdvcmxkIQ=="
#       },
#       "similarity": 0.9795918367346939
#     }
#   ]
# }
```

## License

MIT.