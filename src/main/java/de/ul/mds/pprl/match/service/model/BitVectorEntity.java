package de.ul.mds.pprl.match.service.model;

import java.util.Objects;

import static java.util.Objects.requireNonNull;

public class BitVectorEntity {

    private String id;

    private String value;

    public BitVectorEntity() {
        this("", "");
    }

    public BitVectorEntity(String id, String value) {
        setId(id);
        setValue(value);
    }

    public String getId() {
        return id;
    }

    public String getValue() {
        return value;
    }

    public void setId(String id) {
        this.id = requireNonNull(id);
    }

    public void setValue(String value) {
        this.value = requireNonNull(value);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BitVectorEntity that = (BitVectorEntity) o;
        return id.equals(that.id) && value.equals(that.value);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, value);
    }

    @Override
    public String toString() {
        return "BitVectorEntity{" +
                "id='" + id + '\'' +
                ", value='" + value + '\'' +
                '}';
    }

}
