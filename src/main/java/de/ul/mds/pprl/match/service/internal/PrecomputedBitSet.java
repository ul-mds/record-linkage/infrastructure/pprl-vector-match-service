package de.ul.mds.pprl.match.service.internal;

import java.util.BitSet;

public record PrecomputedBitSet(BitSet bs, int cardinality) {

    public static PrecomputedBitSet from(BitSet bs) {
        return new PrecomputedBitSet(bs, bs.cardinality());
    }

}
