package de.ul.mds.pprl.match.service;

import de.ul.mds.pprl.match.service.handler.MatchHandler;
import io.javalin.Javalin;
import org.slf4j.*;

import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.*;
import java.util.function.Function;

import static java.lang.String.format;

public class MatchApplication {

    private static final Logger LOGGER = LoggerFactory.getLogger(MatchApplication.class);

    private final Javalin app;

    public MatchApplication(MatchApplicationProperties props) {
        if (props.batchSize() < 1) {
            throw new IllegalArgumentException("batch size cannot be lower than one");
        }

        if (!(props.maxComparisonCount() > 0) && !(props.maxComparisonCount() == -1)) {
            throw new IllegalArgumentException("maximum comparison count must be a positive number or -1");
        }

        if (!props.useAvailableCoreCount() && props.workerThreadCount() < 1) {
            throw new IllegalArgumentException("worker thread count must be positive if the available core count is not to be used");
        }

        final int threadCount = props.useAvailableCoreCount() ? Runtime.getRuntime().availableProcessors() : props.workerThreadCount();

        ExecutorService matchExecutor = Executors.newFixedThreadPool(threadCount);

        app = Javalin.create()
                .post("/", new MatchHandler(props, matchExecutor));
    }

    public Javalin app() {
        return app;
    }

    public static void main(String[] args) {
        Properties props = new Properties();

        try {
            props.load(MatchApplication.class.getResourceAsStream("/app.properties"));
        } catch (IOException e) {
            LOGGER.warn("application properties file not found, using defaults", e);
        }

        int batchSize = readProperty(props, "batch-size", Integer::parseUnsignedInt);
        long maxComparisonCount = readProperty(props, "max-comparison-count", Long::parseLong);
        boolean useAvailableCoreCount = readProperty(props, "use-available-core-count", Boolean::parseBoolean);
        int workerThreadCount = readProperty(props, "worker-thread-count", Integer::parseUnsignedInt);

        var appProps = new MatchApplicationProperties(
                batchSize,
                maxComparisonCount,
                useAvailableCoreCount,
                workerThreadCount
        );

        MatchApplication srv;

        try {
            srv = new MatchApplication(appProps);
        } catch (IllegalArgumentException ex) {
            LOGGER.error("invalid configuration", ex);
            throw new RuntimeException(ex);
        }

        LOGGER.info("starting server with properties {}", appProps);

        srv.app().start(8080);
    }

    private static <T> T readProperty(Properties propFile, String propName, Function<String, T> propFunc) {
        String propVal = System.getProperty(format("service.%s", propName), propFile.getProperty(propName));

        if (propVal == null) {
            String msg = format("property %s isn't set in application or system properties", propName);

            LOGGER.error(msg, propFile);
            throw new RuntimeException(msg);
        }

        try {
            return propFunc.apply(propVal);
        } catch (RuntimeException ex) {
            String msg = format("failed to parse value %s for property %s", propVal, propName);

            LOGGER.error(msg, ex);
            throw new RuntimeException(msg, ex);
        }
    }

}
