package de.ul.mds.pprl.match.service.model;

import java.util.Objects;

import static java.util.Objects.requireNonNull;

public class MatchConfiguration {

    private String measure;

    private double threshold;

    public MatchConfiguration() {
        this("", 0d);
    }

    public MatchConfiguration(String measure, double threshold) {
        setMeasure(measure);
        setThreshold(threshold);
    }

    public String getMeasure() {
        return measure;
    }

    public void setMeasure(String measure) {
        this.measure = requireNonNull(measure);
    }

    public double getThreshold() {
        return threshold;
    }

    public void setThreshold(double threshold) {
        this.threshold = threshold;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MatchConfiguration that = (MatchConfiguration) o;
        return Double.compare(that.threshold, threshold) == 0 && measure.equals(that.measure);
    }

    @Override
    public int hashCode() {
        return Objects.hash(measure, threshold);
    }

    @Override
    public String toString() {
        return "MatchConfiguration{" +
                "measure='" + measure + '\'' +
                ", threshold=" + threshold +
                '}';
    }

}
