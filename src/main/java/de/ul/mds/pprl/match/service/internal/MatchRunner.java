package de.ul.mds.pprl.match.service.internal;

import de.hs_mittweida.dbs.matching.algorithm.SetSimilarityFunction;
import de.ul.mds.pprl.match.service.model.*;

import java.util.*;

public class MatchRunner implements Runnable {

    private final SetSimilarityFunction fn;

    private double threshold;

    private final Map<String, PrecomputedBitSet> lookupMap;

    private final Queue<MatchedBitVectorPair> matchList;

    private final List<BitVectorEntity> domainList;

    private final List<BitVectorEntity> rangeList;

    public MatchRunner(SetSimilarityFunction fn, double threshold, Map<String, PrecomputedBitSet> lookupMap, Queue<MatchedBitVectorPair> matchList, List<BitVectorEntity> domainList, List<BitVectorEntity> rangeList) {
        this.fn = fn;
        this.threshold = threshold;
        this.lookupMap = lookupMap;
        this.matchList = matchList;
        this.domainList = domainList;
        this.rangeList = rangeList;
    }

    @Override
    public void run() {
        for (var domain : domainList) {
            for (var range : rangeList) {
                var d = lookupMap.get(domain.getValue());
                var r = lookupMap.get(range.getValue());

                double sim = fn.compute(intersectCardinality(d.bs(), r.bs()), d.cardinality(), r.cardinality());

                if (sim >= threshold) {
                    matchList.add(new MatchedBitVectorPair(domain, range, sim));
                }
            }
        }
    }

    int intersectCardinality(BitSet b1, BitSet b2) {
        BitSet intersect = (BitSet) b2.clone();
        intersect.and(b1);

        return intersect.cardinality();
    }

}
