package de.ul.mds.pprl.match.service.model;

import java.util.*;

import static java.util.Objects.requireNonNull;

public class MatchResponse {

    private MatchConfiguration config;

    private Collection<MatchedBitVectorPair> matches;

    public MatchResponse() {
        this(new MatchConfiguration(), new ArrayList<>());
    }

    public MatchResponse(MatchConfiguration config, Collection<MatchedBitVectorPair> matches) {
        setConfig(config);
        setMatches(matches);
    }

    public MatchConfiguration getConfig() {
        return config;
    }

    public void setConfig(MatchConfiguration config) {
        this.config = requireNonNull(config);
    }

    public Collection<MatchedBitVectorPair> getMatches() {
        return matches;
    }

    public void setMatches(Collection<MatchedBitVectorPair> matches) {
        this.matches = requireNonNull(matches);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MatchResponse that = (MatchResponse) o;
        return config.equals(that.config) && matches.equals(that.matches);
    }

    @Override
    public int hashCode() {
        return Objects.hash(config, matches);
    }

    @Override
    public String toString() {
        return "MatchResponse{" +
                "config=" + config +
                ", matches=" + matches +
                '}';
    }

}
