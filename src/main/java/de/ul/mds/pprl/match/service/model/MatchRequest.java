package de.ul.mds.pprl.match.service.model;

import java.util.*;

import static java.util.Objects.requireNonNull;

public class MatchRequest {

    private MatchConfiguration config;

    private List<BitVectorEntity> domain;

    private List<BitVectorEntity> range;

    public MatchRequest() {
        this(new MatchConfiguration(), new ArrayList<>(), new ArrayList<>());
    }

    public MatchRequest(MatchConfiguration config, List<BitVectorEntity> domain, List<BitVectorEntity> range) {
        setConfig(config);
        setDomain(domain);
        setRange(range);
    }

    public MatchConfiguration getConfig() {
        return config;
    }

    public void setConfig(MatchConfiguration config) {
        this.config = requireNonNull(config);
    }

    public List<BitVectorEntity> getDomain() {
        return domain;
    }

    public void setDomain(List<BitVectorEntity> domain) {
        this.domain = requireNonNull(domain);
    }

    public List<BitVectorEntity> getRange() {
        return range;
    }

    public void setRange(List<BitVectorEntity> range) {
        this.range = requireNonNull(range);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MatchRequest that = (MatchRequest) o;
        return config.equals(that.config) && domain.equals(that.domain) && range.equals(that.range);
    }

    @Override
    public int hashCode() {
        return Objects.hash(config, domain, range);
    }

    @Override
    public String toString() {
        return "MatchRequest{" +
                "config=" + config +
                ", domain=" + domain +
                ", range=" + range +
                '}';
    }

}
