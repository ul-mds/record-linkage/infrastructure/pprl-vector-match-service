package de.ul.mds.pprl.match.service;

public record MatchApplicationProperties(
        int batchSize,
        long maxComparisonCount,
        boolean useAvailableCoreCount,
        int workerThreadCount
) {
}
