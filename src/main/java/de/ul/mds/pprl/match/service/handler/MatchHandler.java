package de.ul.mds.pprl.match.service.handler;

import de.hs_mittweida.dbs.matching.algorithm.SetSimilarityFunction;
import de.ul.mds.pprl.match.service.MatchApplicationProperties;
import de.ul.mds.pprl.match.service.internal.*;
import de.ul.mds.pprl.match.service.model.*;
import io.javalin.http.*;
import io.javalin.validation.*;
import org.jetbrains.annotations.NotNull;
import org.slf4j.*;

import java.math.BigInteger;
import java.util.*;
import java.util.concurrent.*;

import static de.ul.mds.pprl.match.service.handler.MatchHandler.Constants.*;
import static java.lang.String.format;
import static java.util.Map.entry;
import static java.util.Objects.requireNonNull;

public class MatchHandler implements Handler {

    interface Constants {

        String MEASURE_DICE = "dice";
        String MEASURE_JACCARD = "jaccard";
        String MEASURE_COSINE = "cosine";

        Set<String> MEASURES = Set.of(MEASURE_DICE, MEASURE_COSINE, MEASURE_JACCARD);

    }

    Map<String, SetSimilarityFunction> similarityFunctionMap = Map.ofEntries(
            entry(MEASURE_DICE, SetSimilarityFunction.DICE),
            entry(MEASURE_COSINE, SetSimilarityFunction.COSINE),
            entry(MEASURE_JACCARD, SetSimilarityFunction.JACCARD)
    );

    private static final Logger LOGGER = LoggerFactory.getLogger(MatchHandler.class);

    private final MatchApplicationProperties props;

    private final ExecutorService matchExecutor;

    private final BigInteger maxComparisonCount;

    public MatchHandler(MatchApplicationProperties props, ExecutorService matchExecutor) {
        this.props = props;
        this.matchExecutor = matchExecutor;

        maxComparisonCount = BigInteger.valueOf(props.maxComparisonCount() == -1 ? Long.MAX_VALUE : props.maxComparisonCount());
    }

    private static <T> ValidationException newBodyValidationException(String message, T value) {
        return new ValidationException(Map.of("REQUEST_BODY", List.of(
                new ValidationError<>(message, Map.of(), value)
        )));
    }

    @Override
    public void handle(@NotNull Context ctx) {
        MatchRequest req = ctx.bodyValidator(MatchRequest.class)
                .check(r -> r.getConfig().getThreshold() >= 0d, "config.threshold must be higher than or equal to zero")
                .check(r -> r.getConfig().getThreshold() <= 1d, "config.threshold must be lower than or equal to one")
                .check(r -> MEASURES.contains(r.getConfig().getMeasure()), format("config.measure is unknown, must be one of \"%s\"",
                        String.join("\", \"", MEASURES.stream()
                                .sorted(String.CASE_INSENSITIVE_ORDER)
                                .toList())))
                .check(r -> !r.getDomain().isEmpty(), "domain must not be empty")
                .check(r -> !r.getRange().isEmpty(), "range must not be empty")
                .get();

        LOGGER.debug("new request with config {}", req.getConfig());

        var domainList = req.getDomain();
        var rangeList = req.getRange();

        // bigints used here because with big lists, we might exceed the memory limits of ints (and longs?) very easily.
        var totalComparisons = BigInteger.valueOf(domainList.size()).multiply(BigInteger.valueOf(rangeList.size()));

        if (!totalComparisons.min(maxComparisonCount).equals(totalComparisons)) {
            throw newBodyValidationException("too many comparisons to perform", req);
        }

        var setSimilarityFn = similarityFunctionMap.get(req.getConfig().getMeasure());

        var bitSetLookupMap = new HashMap<String, PrecomputedBitSet>(domainList.size() + rangeList.size());

        for (var d : domainList) populateBitSetLookupMap(req, bitSetLookupMap, d);
        for (var r : rangeList) populateBitSetLookupMap(req, bitSetLookupMap, r);

        var matchList = new ConcurrentLinkedQueue<MatchedBitVectorPair>();
        var futureList = new ArrayList<CompletableFuture<Void>>();

        for (int i = 0; i < domainList.size(); i += props.batchSize()) {
            for (int j = 0; j < rangeList.size(); j += props.batchSize()) {
                int iMax = Math.min(i + props.batchSize(), domainList.size());
                int jMax = Math.min(j + props.batchSize(), rangeList.size());

                futureList.add(CompletableFuture.runAsync(new MatchRunner(
                        setSimilarityFn,
                        req.getConfig().getThreshold(),
                        bitSetLookupMap,
                        matchList,
                        domainList.subList(i, iMax),
                        rangeList.subList(j, jMax)
                ), matchExecutor));
            }
        }

        CompletableFuture.allOf(futureList.toArray(new CompletableFuture[0])).join();

        ctx.json(new MatchResponse(req.getConfig(), matchList));
    }

    void populateBitSetLookupMap(MatchRequest req, Map<String, PrecomputedBitSet> lookupMap, BitVectorEntity entity) {
        byte[] bsBytes;

        try {
            bsBytes = Base64.getUrlDecoder().decode(entity.getValue());
        } catch (IllegalArgumentException ex) { // invalid base64
            throw newBodyValidationException(format("vector with ID \"%s\" is not valid URL-safe Base64", entity.getId()), req);
        }

        lookupMap.put(entity.getValue(), PrecomputedBitSet.from(BitSet.valueOf(bsBytes)));
    }

}
