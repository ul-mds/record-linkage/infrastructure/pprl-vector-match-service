package de.ul.mds.pprl.match.service.model;

import java.util.Objects;

import static java.util.Objects.requireNonNull;

public class MatchedBitVectorPair {

    private BitVectorEntity domain;

    private BitVectorEntity range;

    private double similarity;

    public MatchedBitVectorPair() {
        this(new BitVectorEntity(), new BitVectorEntity(), 0d);
    }

    public MatchedBitVectorPair(BitVectorEntity domain, BitVectorEntity range, double similarity) {
        setDomain(domain);
        setRange(range);
        setSimilarity(similarity);
    }

    public BitVectorEntity getDomain() {
        return domain;
    }

    public void setDomain(BitVectorEntity domain) {
        this.domain = requireNonNull(domain);
    }

    public BitVectorEntity getRange() {
        return range;
    }

    public void setRange(BitVectorEntity range) {
        this.range = requireNonNull(range);
    }

    public double getSimilarity() {
        return similarity;
    }

    public void setSimilarity(double similarity) {
        this.similarity = similarity;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MatchedBitVectorPair that = (MatchedBitVectorPair) o;
        return Double.compare(that.similarity, similarity) == 0 && domain.equals(that.domain) && range.equals(that.range);
    }

    @Override
    public int hashCode() {
        return Objects.hash(domain, range, similarity);
    }

    @Override
    public String toString() {
        return "MatchedBitVectorPair{" +
                "domain=" + domain +
                ", range=" + range +
                ", similarity=" + similarity +
                '}';
    }

}
