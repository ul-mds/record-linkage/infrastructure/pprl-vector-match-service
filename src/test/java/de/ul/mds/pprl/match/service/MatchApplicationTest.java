package de.ul.mds.pprl.match.service;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class MatchApplicationTest {

    @Test
    public void testThrowOnBatchSizeTooSmall() {
        var ex = assertThrows(IllegalArgumentException.class, () -> new MatchApplication(
                new MatchApplicationProperties(0, -1, true, 2)
        ));

        assertThat(ex.getMessage()).isEqualTo("batch size cannot be lower than one");
    }

    @Test
    public void testThrowOnNegativeComparisonCount() {
        var ex = assertThrows(IllegalArgumentException.class, () -> new MatchApplication(
                new MatchApplicationProperties(100, -2, true, 2)
        ));

        assertThat(ex.getMessage()).isEqualTo("maximum comparison count must be a positive number or -1");
    }

    @Test
    public void testThrowOnZeroComparisonCount() {
        var ex = assertThrows(IllegalArgumentException.class, () -> new MatchApplication(
                new MatchApplicationProperties(100, 0, true, 2)
        ));

        assertThat(ex.getMessage()).isEqualTo("maximum comparison count must be a positive number or -1");
    }

    @Test
    public void testThrowOnNonPositiveWorkerCountIfNoAutoConfigure() {
        var ex = assertThrows(IllegalArgumentException.class, () -> new MatchApplication(
                new MatchApplicationProperties(100, -1, false, 0)
        ));

        assertThat(ex.getMessage()).isEqualTo("worker thread count must be positive if the available core count is not to be used");
    }

}
