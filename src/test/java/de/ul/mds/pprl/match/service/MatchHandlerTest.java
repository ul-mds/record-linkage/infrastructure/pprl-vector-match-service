package de.ul.mds.pprl.match.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.*;
import de.ul.mds.pprl.match.service.model.*;
import io.javalin.Javalin;
import org.junit.jupiter.api.Test;

import java.util.*;
import java.util.random.RandomGenerator;
import java.util.stream.IntStream;

import static io.javalin.testtools.JavalinTest.test;
import static java.lang.String.format;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.fail;

public class MatchHandlerTest {

    static final ObjectMapper MAPPER = new ObjectMapper();

    static final RandomGenerator RNG = RandomGenerator.getDefault();

    static BitVectorEntity nextVector() {
        byte[] randBytes = new byte[16];
        RNG.nextBytes(randBytes);

        return new BitVectorEntity(String.valueOf(RNG.nextInt()),
                Base64.getUrlEncoder().encodeToString(randBytes));
    }

    static List<String> collectRequestBodyErrorsFrom(String s) {
        try {
            JsonNode node = MAPPER.readValue(s, JsonNode.class);
            var errorMessages = new ArrayList<String>();

            for (JsonNode errorNode : node.get("REQUEST_BODY")) {
                errorMessages.add(errorNode.get("message").asText());
            }

            return errorMessages;
        } catch (JsonProcessingException e) {
            fail("response body is not valid JSON");
        }

        return List.of();
    }

    private static Javalin app() {
        return app(new MatchApplicationProperties(100, -1, true, 2));
    }

    private static Javalin app(MatchApplicationProperties props) {
        return new MatchApplication(props).app();
    }

    @Test
    public void testBadRequestOnThresholdTooLow() {
        test(app(), (server, client) -> {
            var resp = client.post("/", new MatchRequest(
                    new MatchConfiguration("jaccard", -0.01d),
                    List.of(),
                    List.of()
            ));

            assertThat(resp.code()).isEqualTo(400);
            assertThat(collectRequestBodyErrorsFrom(resp.body().string())).contains("config.threshold must be higher than or equal to zero");
        });
    }

    @Test
    public void testBadRequestOnThresholdTooHigh() {
        test(app(), (server, client) -> {
            var resp = client.post("/", new MatchRequest(
                    new MatchConfiguration("jaccard", 1.01d),
                    List.of(),
                    List.of()
            ));

            assertThat(resp.code()).isEqualTo(400);
            assertThat(collectRequestBodyErrorsFrom(resp.body().string())).contains("config.threshold must be lower than or equal to one");
        });
    }

    @Test
    public void testBadRequestOnUnknownMeasure() {
        test(app(), (server, client) -> {
            var resp = client.post("/", new MatchRequest(
                    new MatchConfiguration("foobar", 1.0d),
                    List.of(),
                    List.of()
            ));

            assertThat(resp.code()).isEqualTo(400);
            assertThat(collectRequestBodyErrorsFrom(resp.body().string())).contains("config.measure is unknown, must be one of \"cosine\", \"dice\", \"jaccard\"");
        });
    }

    @Test
    public void testBadRequestOnDomainEmpty() {
        test(app(), (server, client) -> {
            var resp = client.post("/", new MatchRequest(
                    new MatchConfiguration("jaccard", 1d),
                    List.of(),
                    List.of(nextVector())
            ));

            assertThat(resp.code()).isEqualTo(400);
            assertThat(collectRequestBodyErrorsFrom(resp.body().string())).contains("domain must not be empty");
        });
    }

    @Test
    public void testBadRequestOnRangeEmpty() {
        test(app(), (server, client) -> {
            var resp = client.post("/", new MatchRequest(
                    new MatchConfiguration("jaccard", 1d),
                    List.of(nextVector()),
                    List.of()
            ));

            assertThat(resp.code()).isEqualTo(400);
            assertThat(collectRequestBodyErrorsFrom(resp.body().string())).contains("range must not be empty");
        });
    }

    @Test
    public void testBadRequestOnInvalidBase64InDomain() {
        test(app(), (server, client) -> {
            var vecCorrect = nextVector();
            var vecIncorrect = nextVector();

            vecIncorrect.setValue(vecIncorrect.getValue() + "ä");

            var resp = client.post("/", new MatchRequest(
                    new MatchConfiguration("jaccard", 1d),
                    List.of(vecIncorrect),
                    List.of(vecCorrect)
            ));

            assertThat(resp.code()).isEqualTo(400);
            assertThat(collectRequestBodyErrorsFrom(resp.body().string())).contains(format("vector with ID \"%s\" is not valid URL-safe Base64",
                    vecIncorrect.getId()));
        });
    }

    @Test
    public void testBadRequestOnInvalidBase64InRange() {
        test(app(), (server, client) -> {
            var vecCorrect = nextVector();
            var vecIncorrect = nextVector();

            vecIncorrect.setValue(vecIncorrect.getValue() + "ä");

            var resp = client.post("/", new MatchRequest(
                    new MatchConfiguration("jaccard", 1d),
                    List.of(vecCorrect),
                    List.of(vecIncorrect)
            ));

            assertThat(resp.code()).isEqualTo(400);
            assertThat(collectRequestBodyErrorsFrom(resp.body().string())).contains(format("vector with ID \"%s\" is not valid URL-safe Base64",
                    vecIncorrect.getId()));
        });
    }

    @Test
    public void testBadRequestOnTooManyComparisons() {
        // set max comparisons to 2
        var props = new MatchApplicationProperties(100, 2, true, 1);
        var vectors = IntStream.range(0, 2)
                .mapToObj(i -> nextVector())
                .toList();

        test(new MatchApplication(props).app(), (server, client) -> {
            var resp = client.post("/", new MatchRequest(
                    new MatchConfiguration("jaccard", 1d),
                    vectors,
                    vectors
            ));

            assertThat(resp.code()).isEqualTo(400);
            assertThat(collectRequestBodyErrorsFrom(resp.body().string())).contains("too many comparisons to perform");
        });
    }

    @Test
    public void testMatch() {
        test(app(), (server, client) -> {
            var vectors = IntStream.range(0, 100)
                    .mapToObj(i -> nextVector())
                    .toList();

            var config = new MatchConfiguration("jaccard", 1d);

            var resp = client.post("/", new MatchRequest(
                    config,
                    vectors,
                    vectors
            ));

            assertThat(resp.code()).isEqualTo(200);
            var r = MAPPER.readValue(resp.body().string(), MatchResponse.class);

            assertThat(r.getConfig()).isEqualTo(config);
            assertThat(r.getMatches()).hasSize(vectors.size());
        });
    }

}
